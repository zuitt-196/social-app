import bcrypt from "bcrypt";
import JWT from 'jsonwebtoken';
import User from "../Models/User.js";

// REGISTER USER
export const registerUser = async (req, res) => {
    try {
        const {firstName, lastName, email, password,picturePath,friends,location} = req.body;

        const salt = await bcrypt.genSalt();
        const passwordHasded = await bcrypt.hash(password, salt);

        const newUser = await new User({
            firstName,
            lastName,
            email,
            password:passwordHasded,
            picturePath,
            friends,
            location,
            viewPofile: Math.round(Math.random() *1000), // random number
            impression: Math.round(Math.random() *1000) // random number
        })

        const saveUser = await newUser.save();
        res.status(201).json(saveUser)
    } catch (error) {
        res.status(500).json({ error: error.message})
    }
}

// LOGDIN USER 
export const loginUser = async(req, res) => {
    // console.log(req.header)
    try {
        const {email , password} = req.body;
        const user  = await User.findOne({email: email})
        if (!user) return res.status(400).json({message: "User does not exist"});
        const isMatch  = await bcrypt.compare(password , user.password);        
        if(!isMatch) return res.status(400).json({message: "User does not exist"});
        
        // GENERATE A TOKEN
        const token = JWT.sign({
            id: user._id,
        }, process.env.SECRET_KEY);
        // DELTE THE PASSWORD AUTOMATICALLY
        delete user.password

        res.status(200).json({token, user});
    } catch (error) {
        res.status(500).json({ error: error.message});
    }
}